//
//  Image.swift
//  Employed
//
//  Created by Anuranjan Bose on 11/06/19.
//  Copyright © 2019 Anuranjan Bose. All rights reserved.
//

import Foundation

struct Image: Decodable {
    var author: String
    var id: Int
}
